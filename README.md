# Vue-Nuxt-analysis😊

----

> - Vue & Nuxt 프레임 워크의 모든 기능을 사용해보고 그에 대한 나의 생각 및 실무에 도입했을 때 어떤 시너지를 낼 수 있을까에 대한 분석 내용이다.
> - 솔직히 사용할 것 같은 것만 쓸꺼임
> - 모든 내용을 순서대로 쓰진 않음
> - 내용은 펙트기반이지만 의견은 내 의견이므로 그냥 참고만
> - 공식문서 내용을 다루고 자주 쓰는 라이브러리도 분석해보려고 함



- **vue-router**

> https://router.vuejs.org/kr/



- **mixin**

  > https://kamang-it.tistory.com/entry/Vue15mixin-%EB%A7%8C%EB%93%A4%EA%B3%A0-%EC%82%AC%EC%9A%A9%ED%95%B4%EB%B3%B4%EC%9E%90%EA%B8%B0%EB%8A%A5%EB%A7%8C-%EC%9E%AC%EC%82%AC%EC%9A%A9%EA%B0%80%EB%8A%A5%ED%95%98%EA%B2%8C-%ED%95%98%EB%8A%94-%EB%B0%A9%EB%B2%95



## 1. Vue 기본 데이터 선언 및  라이프사이클 훅       

- 기본적인 Vue 파일 형식
- Vue의 핵심 요소 및 이슈 발생인 **라이프사이클 훅에 대한 설명을 알아보자**
- `update`, `destoryed` 에 대한 내용은 제외 시킴  => 현재는 안 쓸 것 같아서

```vue
<template> 
<!--항상 template 첫 번째 자식태그는 하나만 존재해야함 / 그 밑에 자식들은 상관없음-->
  <div class="container">
     
  </div>
</template>

<script>
export default {    
    beforeCreate() {
        // data, 이벤트, watch 등이 설정 안된 구간
        // 솔직히 쓸모없다고 느낌 => 페이지 잘 열렸다 그정도 ?
    },
    created() {
		//data, computed, methods, watch 등의 정의된 것은 사용 가능한 구간
        // 그러니 DOM이 생성되지 않아 DOM 조작과 관련된 것은 나타나지 않는다
        // DOM에 뿌려야할 데이터가 있다면 이 단계에서 처리하는게 좋을 듯 하다
    },
    beforeMout() {
        // DOM 바로 생기기 직전 구간
        // 솔직히 created 있어서 필요 없음 
        // 굳이 데이터의 순서를 지키고 싶으면 쓰던지
    },
    mounted() {
	    //DOM 조작이 가능한 단계
        //근데 모든 컴포넌트가 생성되었다고 할 수 없다 
        //그래서 $nextTick이라는 기능이 있다
        this.$nextTick(() => {
            console.log("해당 단계에서는 마운트가 끝나고 실행됨")
        })
        console.log("m-n-after-code") 
        // 그렇기 때문에 코드를 뒤에 적어도 nextTick 단계가 뒤에 일어난다
    },
}
</script>

<style>

</style>
```



### 	부모/자식 컴포넌트 에서  created, mounted 의 순서에 특이점이 있음

- created 순서 부모 => 자식
- mounted 순서 자식 => 부모



## 2. Vue 템플릿 문법



### 	기본적으로 데이터 바인딩은 이중 중괄호로 나타낸다

- `v-once` 같이 고정된 데이터 바인딩도 할 수 있지만 쓸 일 없다

```vue
<h1>{{ data }}</h1>
```



### 	만약 html 태그를 넣고 싶다면  v-html을 사용하면 됨

- h2_data : <h2>inner data<h2>

```vue
<h1 v-html="h2_data"></h1>

<!--실제 아래와 같이 나타남-->
<h1>
    <h2>inner data<h2>
</h1>
```



### 	 이중 중괄호 안에는  자바스크립트 모든 표현식을 사용할 수 있음

- 어지간하면 스크립트 구간에서 처리하면 좋겠지만, 편의성을 위해 사용할 수도 있다고 생각함

```vue
<div>{{ user_nm }}</div>
<div v-html="h2_data"></div>
<div>{{ num + 5 }}</div>
<div>{{ false ? 'OK':'NK' }}</div>
<div>{{ time.split('-')[1] }}</div>
```



## 3. 디렉티브

- `v-` 가 붙는 특수 속성
- vue 쓰면서 안 쓸 수가 없음 
- 자주 쓰는 것들은 `v-if`, `v-else-if` , `v-else`, `v-model`, `v-html` , `v-for`, `v-bind(:)`,`v-on(a)`

### 3-1  v-if  / v-else-if/v-else

- 비슷한 디렉티브로 `v-show`가 있는데, `v-show`는 렌더링 되고 DOM에 남아있음
- 그냥 v-if 쓰자 이말 

```vue
<template>
  <div>
	<div v-if="IsInit == 1">V-IF 테스트 </div>
   	<div v-else-if="IsInit == 2">V-ELSE-IF 테스트</div>
   	<div v-else>V-ELSE 테스트</div> <!--얘가 나온다-->
  </div>
</template>
<script>
    export default {
        data() {
            return {
                IsInit : 3
            }
        }

    }
</script>
```



### 3-2  v-for

- `key`를 통해서 고유값을 가지게 한다 ? 뭐 그렇게 생각한게 편할듯 => 2.2 이상부터

```vue
<template>
  <div>
    <ul v-for="(item,index) in items" :key="item.age">
        <li>{{ item.key}} + {{ index }} + {{item.age}}</li>
    </ul>
  </div>
</template>
<script>
    export default {
        data() {
            return {

                items: [
                    { key: '키키', age: 10 },
                    { key: '히히', age: 11 }
                ]                
            }
        }

    }
</script>
```



#### 공식문서에 보면  v-for와 v-if를 동일 tag에  쓰는 것을 추천하지 않는다

```vue
<li v-for="todo in todos" v-if="!todo.isComplete">
  {{ todo }}
</li>
```



#### v-for를 통해서 컴포넌트 `props` 도 가능하다

```vue
<my-component v-for="item in items" :key="item.id"></my-component>
```



### 3-3 v-on / @ (약어)

- 이벤트를 작동시키게 하는 디렉티브이다
- 일반적으로 `v-on` 보다는 `@`를 붙여서 쓴다

```vue
<template>
  <div>
      <div>Watch</div>
      <div>{{ watchData }}</div>
      <button @click="procWatch">watch 가동버튼</button>
      <button @keypress.enter="procWatch">watch 가동버튼</button>
  </div>
</template>
<script>
export default {
    data() {
        return {
            watchData : 0
        }
    },
    methods: {
        procWatch() {
            this.watchData += 1
        }
    }    
}
</script>
```



#### 이벤트의 키 수식어로는 다음과 같다

- `enter` , `tab` ,`delete`  등등 => 어지간한거 다됨

### 

### 3-4  v-model

- `input`, `textarea`에 사용하는 디렉티브
- `v-model.lazy`, `v-model.trim` , `v-model.number` 등으로 사소한 처리를 할 수 있음

```vue
<template>
  <div>
    <div>
        <input @keypress.enter="alertM" v-model="textData" type="text">
    </div>
      
     <!--이런식으로 쓰기도 함-->
    <select v-model="selected" multiple>
    <option>A</option>
    <option>B</option>
    <option>C</option>
    </select>    

    <br>
    <span>Selected: {{ selected }}</span>   
      
  </div>
</template>
<script>
    export default {
        data() {
            return {
                textData: '',
                selected : '',         
            }
        },
        methods: {
            alertM() {
                alert(this.textData)
                this.textData = ''
            }
        }

    }
</script>
```





## 4. Computed - Watch

### 4-1 Computed 

- 템플릿 문법으로 js를 활용하면 가독성도 좋지 않아서 어지간하면 `script` 단에서 처리하는게 좋다
- 이미 `data` 에 정의된 것을 가공한다 생각하면 된다.
- `methods`에서 정의해서 return 할 수도 있다

```vue
<template>
  <div>
      <h1>{{ dummy }}</h1>
      <h2>{{ golping }}</h2>
      <h3>{{ returnData() }}</h3>
  </div>
</template>

<script>
export default {
    data() {
        return {
            dummy : "개발자"
        }
    },
    computed: {
        golping() {
            return this.dummy + 'golping'
        }
    },
    methods: {
        returnData() {
            return this.dummy + '골핑'
        }
    }    
}
</script>
```



**4-2 watch**

- 특정데이터가 변화가 감지될 때 메서드를 실행시킬 수 있다
- `computed`와 비슷하지만 소프트웨어 공학적 측면에서는 다름
- `watch`는 **명령형 프로그래밍 방식**이라면,  `computed`는 **선언형 프로그래밍 방식**이다
- 생각보다 언제 써야할 지 까다롭다 => 조건없이 쓸 경우 무한루프를 돌기 때문에 무조건 분기가 필요

```vue
<template>
  <div>
      <div>Watch</div>
      <div>{{ watchData }}</div>
      <button @click="procWatch">watch 가동버튼</button>
  </div>
</template>

<script>
export default {
    data() {
        return {
            watchData : 0
        }
    },
    watch: {
        watchData() {
            if (this.watchData > 5)  {
                console.log("무야호")
            } else {
                this.watchData *= 2
            }
        }
    },
    methods: {
        returnData() {
            return this.dummy + '골핑'
        },
        procWatch() {
            this.watchData += 1
        }
    }    
}
</script>
```



## 5. 클래스와 스타일 바인딩

- 클래스 자체에 `boolean` 형태로  `class` 변경으로 스타일을 동적으로 변경할 수 있음
- 스타일도 인라인 형태로 `data`와 연동해서 동적으로 사용할 수 있음

```vue
<template>
  <div>
    <h1 class="test" :class="{ red:IsRed , blue:IsBlue, green:IsGreen }">텍스트 글자 변경할꺼임</h1>
    <button @click="Red">빨간색</button>
    <button @click="Blue">파란색</button>
    <button @click="Green">초록색</button>
    <button @click="init">원래대로</button>
    
    <h3>스타일 바인딩</h3>
    <div :style="{ color:TextRed }">스타일 인라인 바인딩 레드</div>
    

  </div>
</template>

<script>
export default {
    data() {
        return {
            IsRed : false,
            IsBlue : false,
            IsGreen : false,
            TextRed : 'red'


        }
    },
    methods: {
        Red() {
            this.IsRed = true
            this.IsBlue = false
            this.IsGreen = false
        },
        Blue() {
            this.IsRed = false
            this.IsBlue = true
            this.IsGreen = false            
        },
        Green() {
            this.IsRed = false
            this.IsBlue = false
            this.IsGreen = true
        },
        init() {
            this.IsRed = false
            this.IsBlue = false
            this.IsGreen = false
        }
    }
}
</script>

<style>
    .red {
        color: red;
    }
    .blue {
         color: blue;
    }
    .green {
         color: green;
    }
</style>
```

